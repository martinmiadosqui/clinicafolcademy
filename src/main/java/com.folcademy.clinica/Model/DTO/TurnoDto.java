package com.folcademy.clinica.Model.DTO;


import com.folcademy.clinica.Model.Entities.Medico;
import com.folcademy.clinica.Model.Entities.Paciente;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


import java.time.LocalDate;
import java.time.LocalTime;



@Data
@AllArgsConstructor
@NoArgsConstructor

public class TurnoDto {
    Integer idTurno;

    LocalDate fecha;

    LocalTime hora;

    Boolean atendido;

    Integer idpaciente;

    Integer idmedico;

}