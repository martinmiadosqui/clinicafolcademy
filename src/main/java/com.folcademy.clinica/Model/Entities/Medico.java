package com.folcademy.clinica.Model.Entities;

import lombok.*;
import org.hibernate.Hibernate;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "medico")
@Getter
@Setter
@ToString
@RequiredArgsConstructor
public class Medico {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idmedico", columnDefinition = "INT(10) UNSIGNED")
     Integer idMedico;
    @Column(name = "nombre")
     String nombre = "";
    @Column(name = "apellido")
     String apellido = "";
    @Column(name = "profesion")
     String profesion = "";
    @Column(name = "consulta")
     Integer consulta = 0;

    @Override
    public boolean equals(Object o){
        if (this == o) return true;
        if (o == null || Hibernate.getClass(this)!=Hibernate.getClass(o)) return false;
        Medico medico= (Medico) o;

        return Objects.equals(idMedico, medico.idMedico);
    }

    @Override
    public int hashCode(){
        return 454545;
    }

}
