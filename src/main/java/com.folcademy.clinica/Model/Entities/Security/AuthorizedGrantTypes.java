/*package com.folcademy.clinica.Model.Entities.Security;

import lombok.*;

import javax.persistence.*;
import java.security.Timestamp;

@Entity
@Table(name="authorized_grant_types")
@Getter
@Setter
@ToString
@RequiredArgsConstructor
public class AuthorizedGrantTypes {
    @Column(name="name")
    String name;
    @Column(name = "deleted",columnDefinition = "TINYINT")
    boolean deleted;
    @Column(name="created")
    private Timestamp created;
    @Column(name="modified")
    private Timestamp modified;
    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private int id;
}
*/