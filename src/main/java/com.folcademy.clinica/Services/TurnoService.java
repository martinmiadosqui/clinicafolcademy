package com.folcademy.clinica.Services;

import com.folcademy.clinica.Excepcions.BadRequestException;
import com.folcademy.clinica.Excepcions.NotFoundException;
import com.folcademy.clinica.Model.DTO.TurnoDto;
import com.folcademy.clinica.Model.DTO.TurnoEnteroDto;
import com.folcademy.clinica.Model.Mappers.TurnoMapper;
import com.folcademy.clinica.Model.Repositories.TurnoRepository;
import org.springframework.stereotype.Service;
import java.util.List;
import java.util.stream.Collectors;

@Service("TurnoService")
public class TurnoService{

    private final TurnoRepository turnoRepository;
    private final TurnoMapper turnoMapper;


    public TurnoService(TurnoRepository turnoRepository, TurnoMapper turnoMapper) {
        this.turnoRepository = turnoRepository;
        this.turnoMapper = turnoMapper;
    }


    public List<TurnoDto> listarTodos(){
        if(turnoRepository.findAll().isEmpty())
            throw new NotFoundException("La lista de turnos esta vacia");
        return turnoRepository.findAll().stream().map(turnoMapper::entityToDto).collect(Collectors.toList());
    }

    public TurnoDto listarUno(Integer id) {
     if(!turnoRepository.existsById(id))
            throw new NotFoundException("No existe el turno");
        return turnoRepository.findById(id).map(turnoMapper::entityToDto).orElse(null);

    }

    public TurnoDto agregar(TurnoDto entity){
     if(entity.getIdTurno()==null)
            throw new BadRequestException("No existe el turno");
        entity.setIdTurno(null);
        return turnoMapper.entityToDto(turnoRepository.save(turnoMapper.dtoToEntity(entity)));
    }


    public TurnoEnteroDto editar(Integer idTurno,TurnoEnteroDto dto){
        if(!turnoRepository.existsById(idTurno))
            throw new NotFoundException("No existe el turno");
        dto.setIdTurno(idTurno);
        return turnoMapper.entityToEnteroDto(
                turnoRepository.save(turnoMapper.enteroDtoToEntity(
                        dto
                ))
        );
    }


    public boolean eliminar(Integer id){
        if(!turnoRepository.existsById(id))
            throw new NotFoundException("No existe el turno");
        turnoRepository.deleteById(id);
        return true;
    }


}
